package timetabling;

import java.util.ArrayList;

import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Sala {
	@CsvField(pos = 1, converterType = IntegerConverter.class)
	private int codigo;
	
	@CsvField(pos = 2)
	private String descricao;
	
	@CsvField(pos = 3, converterType = IntegerConverter.class)
	private int tipo;
	
	@CsvField(pos = 4, converterType = IntegerConverter.class)
	private int capacidade;
	
	private ArrayList<Integer> horarios_ocupada = new ArrayList<Integer>();
	
//	public Sala(int codigo, String descricao, String tipo, int capacidade) {
//		super();
//		this.codigo = codigo;
//		this.descricao = descricao;
//		this.tipo = tipo;
//		this.capacidade = capacidade;
//	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public ArrayList<Integer> getHorarios_ocupada() {
		return horarios_ocupada;
	}
	public void setHorario_ocupada(int horarios_ocupada) {
		this.horarios_ocupada.add(horarios_ocupada);
	}
}
