package timetabling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.jsefa.Deserializer;
import org.jsefa.Serializer;
import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.CsvIOFactory;
import org.jsefa.csv.config.CsvConfiguration;

public class TimetablingRun {        
//    //Função de Aptidão
//    public float funcao_aptidao(Gene[][] gene){
//            
//        int i ,j;
//        int nota_max = 1000;
//        float nota = 0;
//        
//        individio1.setFitness(nota_max);
//        nota = individio1.getFitness();
//
//        for ( i = 0; i < 167; i++) {
//            for(j = 0; j < 31; j++){
//                if(gene[i][j] == null) // era pra testar se o gene (timeslot está vazio)
//                    nota = (float) (nota - 0.1);
//            }
//
//        }
//        individio1.setFitness(nota);
//
//        return individio1.getFitness(); 
//    }
    
//    //função de combinação - ONE-POINT CROSSOVER
//    public Cromossomo combina_gene(Gene[][] g1, Gene[][] g2){
//    	Cromossomo filho;
//    	int i, j;
//    	
//        for(int i = 0 ; i < 15; i++){
//            for(int j = 0 ; j < 168; j++){
//            	filho.colocarNaPosicao(j, i, g1[j][i]);
//            }
//        }
//        
//        for(int i = 16 ; i < 132; i++){
//            for(int j = 0 ; j < 168; j++){
//            	filho.colocarNaPosicao(j, i, g2[j][i]);
//            }
//        }
//    	
//    	return filho;
//    	
//    	//o gene filho tem 50% inicial de um gene pai e 50% do outro gene pai
//    }
//    
//    
//    //função de mutação
//    
//    public Cromossomo muta_gene(Gene[][] gene){
//    	Cromossomo temp;
//    	
//    	
//    	
//		return null;	
//    }

	public static void main(String[] args) throws IOException{
		
		int[] turnos_ec = {1,3};
		int[] turnos_ee = {1,2};
		int[] turnos_em = {3};
		
		int[] horarios_ec = {32,33,34,35,36,37,43,45,45,46,56,57,58,59,60,61,67,68,69,70,80,81,82,83,84,85,91,92,93,94,104,105,106,107,108,109,115,116,117,118,128,129,130,131,132,133,139,140,141,142,152,153,154,155,156,157};
		int[] horarios_ee = {32,33,34,35,36,37,38,39,40,41,42,43,45,45,46,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,152,153,154,155,156,157};
		int[] horarios_em = {43,44,45,46,67,68,69,70,91,92,93,94,115,116,117,118,139,140,141,142,152,153,154,155,156,157};
		
		
		Curso ec = new Curso(1,"Engenhaira de Computação",turnos_ec,horarios_ec);
		Curso ee = new Curso(2,"Engenharia Elétrica",turnos_ee, horarios_ee);
		Curso em = new Curso(3,"Engenharia Mecânica",turnos_em,horarios_em);
		
		//Curso[] curso_todos = {ec, ee, em};
		
		Populacao populacao = new Populacao();
        Cromossomo c1 = new Cromossomo();
        c1.ler_arquivo();
        c1.gera_cromossomo();
        //c1.mostrarHorario();
        c1.mostrarEstrutura();
        c1.fitness();

	}	
}
