package timetabling;

import java.util.ArrayList;

import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Professor {
	
	@CsvField(pos = 1, converterType = IntegerConverter.class)
	private int codigo;
	@CsvField(pos = 2)
	private String nome;
	
	private ArrayList<Integer> disciplinas_a_ministrar = new ArrayList<Integer>();
	private boolean restricao_horario;
	private int[] horarios_nao_pode;
	private int[] horarios_ocupados;
	
	@CsvField(pos = 3)
	public String descricao;
	@CsvField(pos = 4, converterType = IntegerConverter.class)
	public int d1;
	@CsvField(pos = 5, converterType = IntegerConverter.class)
	public int d2;
	@CsvField(pos = 7, converterType = IntegerConverter.class)
	public int d3;
	@CsvField(pos = 8, converterType = IntegerConverter.class)
	public int d4;
	@CsvField(pos = 9, converterType = IntegerConverter.class)
	public int d5;
	
	public void consertar_disciplinas_ministrar(){
		if(this.d1 != 0){
			this.disciplinas_a_ministrar.add(this.d1);
		}
		if(this.d2 != 0){
			this.disciplinas_a_ministrar.add(this.d2);
		}
		if(this.d3 != 0){
			this.disciplinas_a_ministrar.add(this.d3);
		}
		if(this.d4 != 0){
			this.disciplinas_a_ministrar.add(this.d4);
		}
		if(this.d5 != 0){
			this.disciplinas_a_ministrar.add(this.d5);
		}
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<Integer> getDisciplinas_a_ministrar() {
		return disciplinas_a_ministrar;
	}
//	public void setDisciplinas_a_ministrar(Disciplina[] disciplinas_a_ministrar) {
//		this.disciplinas_a_ministrar = disciplinas_a_ministrar;
//	}
	public boolean isRestricao_horario() {
		return restricao_horario;
	}
	public void setRestricao_horario(boolean restricao_horario) {
		this.restricao_horario = restricao_horario;
	}
	public int[] getHorarios_nao_pode() {
		return horarios_nao_pode;
	}
	public void setHorarios_nao_pode(int[] horarios_nao_pode) {
		this.horarios_nao_pode = horarios_nao_pode;
	}
	public int[] getHorarios_ocupados() {
		return horarios_ocupados;
	}
	public void setHorarios_ocupados(int[] horarios_ocupados) {
		this.horarios_ocupados = horarios_ocupados;
	}
}
