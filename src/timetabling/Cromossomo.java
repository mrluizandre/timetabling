package timetabling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;
import org.jsefa.Deserializer;
import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.CsvIOFactory;
import org.jsefa.csv.config.CsvConfiguration;

public class Cromossomo {
	
	// INICIO GETTERS AND SETTERS
	ArrayList<Disciplina> disciplinas =  new ArrayList<Disciplina>();
	ArrayList<Disciplina>  disciplinas_ec = new ArrayList<Disciplina>();
	ArrayList<Disciplina>  disciplinas_ee = new ArrayList<Disciplina>();
	ArrayList<Disciplina> disciplinas_em = new ArrayList<Disciplina>();
	ArrayList<Professor> professores = new ArrayList<Professor>();
	ArrayList<Sala> salas = new ArrayList<Sala>();
    ArrayList<Estudante> estudantes = new ArrayList<Estudante>();
	Gene[][] gene = new Gene[168][32];
	public float fitness;
	public int disciplicas_alocadas = 0;
	public int aulas_cursadas_total = 0;

    //Popula o cromossomo com genes vazios
    public Cromossomo() {
        for(int i = 0 ; i < 32; i++){
            for(int j = 0 ; j < 168; j++){       
                this.gene[j][i] = new Gene();
            }
        }
    }
    public void setFitness(float fitness){
        this.fitness = fitness;
    }
    public float getFitness() {
        return fitness;
    }
    public void setEstudantes(Estudante estudante) {
        	this.estudantes.add(estudante);        
    }
	public void setSalas(Sala sala) {
		this.salas.add(sala);
	}
	public void setProfessor(Professor professor) {
		this.professores.add(professor);
	}
	public ArrayList<Disciplina> getEc() {
		for(Disciplina d: disciplinas_ec){
			System.out.println(d.getDescricao());
		}
		return disciplinas_ec;
	}
	public ArrayList<Disciplina> getEe() {
		return disciplinas_ee;
	}
	public ArrayList<Disciplina> getEm() {
		return disciplinas_em;
	}
	public void setDisciplinas(Disciplina disciplina) {
		
		if(disciplina.getCurso().getCodigo() == 1){
			disciplinas_ec.add(disciplina);
		} else if (disciplina.getCurso().getCodigo() == 2){
			disciplinas_ee.add(disciplina);
		}else if (disciplina.getCurso().getCodigo() == 3){
			disciplinas_em.add(disciplina);
		}
	}
	public Gene[][] getGene() {
		return gene;
	}
	public void setGene(Gene[][] gene) {
		this.gene = gene;
	}
	// FIM GETTERS AND SETTERS
	
	//insere um gene no seu devido lugar no array
	public void colocarNaPosicao(int x, int y, Timeslot timeslot){
	    this.gene[x][y].setTimeslot(timeslot);
	}
	
	//exibi número de disciplinas por curso
	public void mostrarDiciplinasPorCurso(){
		System.out.println("Engenharia de Computação" + disciplinas_ec.size());
		System.out.println("Engenharia de Elétrica" + disciplinas_ee.size());
		System.out.println("Engenharia de Mecânica" + disciplinas_em.size());
	}
	
	//exibe estatísticas
	public void mostrarEstatisticasDisciplinaCurso(Curso[] cursos){
		for(Disciplina d: this.disciplinas){
			System.out.println(d.getDescricao() + " - " + d.getNumeroPedidos() + " pedido(s).");
		}
	}

	//gera estatísticas de disciplinas e cursos
//	public void gerarEstasticasDisciplinaCurso(ArrayList<Estudante> estudantes){
//		for(Estudante e: estudantes){
//			for(Disciplina d: e.getDisciplinas_a_cursar()){
//				d.setNumeroPedidos(d.getNumeroPedidos() + 1);
//			}
//		}
//	}
	
	//ordena disciplinas num vetor em ordem decrescente
	public void ordernarDisciplinas(){
		ordena(disciplinas_ec);
		ordena(disciplinas_ee);
		ordena(disciplinas_em);
	}
	
	//ordena o array de disciplinas recebido
	private void ordena(ArrayList<Disciplina>  disciplinas){
        Collections.sort (disciplinas, new Comparator() {
            public int compare(Object o1, Object o2) {
                Disciplina d1 = (Disciplina) o1;
                Disciplina d2 = (Disciplina) o2;
                return d2.getNumeroPedidos() < d1.getNumeroPedidos() ? -1 : (d2.getNumeroPedidos() >d1.getNumeroPedidos() ? +1 : 0);
            }
        });
		
		for(Disciplina d: disciplinas){
			System.out.println(d.getDescricao() + " tem " + d.getNumeroPedidos() + " pedidos");	
		}
	}
    
	//calcula em que período absuluto (soma dos 3 cursos) a disciplina está, para inserção na matriz
    private int calculaPeriodo(Curso curso, Disciplina disciplina){
        int i = 0;
        switch(curso.getCodigo()){
            case 1:
                i = (disciplina.getPeriodo() - 1);
                System.out.println(i);
                break;
            case 2:
                i =  ((disciplina.getPeriodo() + 12) - 1);
                System.out.println(i);
                break;
            case 3:
                i =((disciplina.getPeriodo() + 22) -1);
                System.out.println(i);
                break;
        }
        System.out.println("CURSO " + curso.getNome() + " CODIGO " + curso.getCodigo() + " PERÍODO " + disciplina.getPeriodo());
        System.out.println("Valor de Y = " + i);
        return i;
    }
	
    //faz preenchimentos das disciplinas no gene
	public void colocarNoHorario(ArrayList<Disciplina>  disciplinas){
		// loop de disciplinas
        for(Disciplina d: disciplinas){
        	
            //System.out.println("FOR DISCIPLINA" + d.getDescricao());
            passa_disciplina:
            	
            // loop de professora que aplicam essa disciplina
            for(Professor p: this.professores){
            	
                if(isInList(p.getDisciplinas_a_ministrar(), d.getCodigo())){
                	//System.out.println("ENTROU NO CONTEM DISCIPLINA");
                	
            	   // exclui dos horáios que o curso pode ser ministrado os horário que este professor não pode
                   int[] horarios = d.getCurso().getTimeslots();
                   horarios = ArrayUtils.removeElements(horarios, p.getHorarios_nao_pode());// p.getHoratios_nao_pode é um índice?
                   
                   //agora procurar salas'
                   for(int h: horarios){
                	   
                	   // loop dos horários acima
                       //System.out.println("FOR HORARIO");
                       
                       for(Sala s: this.salas){
                    	   
                           //System.out.println("FOR SALA");
                           
                           // agora encaixa os estudantes
                           if(!isInList(s.getHorarios_ocupada(),h)){
                        	   
                               ArrayList<Estudante> estudante_gene = new ArrayList<Estudante>();
                               for(Estudante e: this.estudantes){
                            	   
                                   // checa se estudante precisa cursar esta disciplina
                                   if(isInList(e.getDisciplinas_a_cursar(),d.getCodigo())){
                                	   
                                       // checa se estudante está ocupado neste horário
                                       if(!isInList(e.getHorarios_ocupados(),h)){
                                           estudante_gene.add(e);
                                           e.setHorario_ocupados(h);
                                           aulas_cursadas_total += 1;
                                       }
                                   }
                                }
                               
                               if(estudante_gene.size() > 0){
	                               Timeslot gene = new Timeslot();
	                               gene.setSala(s);
	                               s.setHorario_ocupada(h);
	                               gene.setDisciplina(d);
	                               gene.setProfessor(p);
	                               gene.setEstudantes(estudante_gene);
	                               
	                               int periodo = this.calculaPeriodo(d.getCurso(), d);
	                               this.colocarNaPosicao(h, periodo, gene);
	                               System.out.println("LOCALIZACAO X = " + h + " Y = " + periodo);
	                               System.out.println(d.getDescricao());
	                               System.out.println(p.getNome());
	                               System.out.println(s.getDescricao());
	                               System.out.println("Estudantes: " + estudante_gene.size());
	                               
	                               disciplicas_alocadas += 1;
                               }
                               
                               //pega outra disciplina
                               break passa_disciplina;
                            }
                        }
                   	}
                }
            } 
        } 
	}
   
	//gera o cromossomo
	public void gera_cromossomo(){
		colocarNoHorario(disciplinas_ec);
		colocarNoHorario(disciplinas_ee);
		colocarNoHorario(disciplinas_em);
	}
	//exibe o cromossomo gerado
    public void mostrarHorario(){
        for(int i = 0 ; i < 32; i++){
            System.out.println("Periodo " +i);
            for(int j = 0 ; j < 168; j++){
                //System.out.println("Timeslot " +j);
                if((gene[j][i] != null)){
                    for(Timeslot t : gene[j][i].getTimeslot()){
                        //System.out.println(t.getDisciplina().getDescricao());
                        //System.out.println(t.getProfessor().getNome());
                        System.out.println(t.getSala().getDescricao());
                        //System.out.println("Estudantes: " + t.getEstudantes().size());
                    }
                }
            }
        }
        System.out.println(disciplicas_alocadas + " DISCIPLINAS ALOCADAS");
        System.out.println(aulas_cursadas_total + " AULAS CURSADAS TOTAL");
    }
	
    public void mostrarEstrutura(){
    	System.out.println("[   ][01][02][03]04][05][06][07][08][09][10][11][12][01][02][03]04][05][06][07][08][09][10][01][02][03][04][05][06][07][08][09][10]");
    	for(int i = 0; i < 168; i++){
    		System.out.print(String.format("[%03d]", i));
    		for(int j = 0; j < 32; j++){
    			if(this.gene[i][j].getTimeslot().size() > 0)
    				System.out.print("[ " + this.gene[i][j].getTimeslot().size() + "]");
    			else
    				System.out.print("[  ]");
        	}
    		System.out.println("");
    	}
    }
    // cria os objetos e os posiciona dentro do cromossomo
    public void criar_objetos(String entidade, String linha){
        CsvConfiguration config = new CsvConfiguration();
        config.setFieldDelimiter(',');
        config.getSimpleTypeConverterProvider().registerConverterType(Integer.class, IntegerConverter.class);
        
		switch (entidade) {
	        case "TIMESLOT":
	            break;
	        case "CURSO":
	            break;
	        case "TIPO DE SALA":
	            break;
	        case "SALA":
	            Deserializer deserializer_sala = CsvIOFactory.createFactory(config,Sala.class).createDeserializer();
	            StringReader reader = new StringReader(linha);
	            deserializer_sala.open(reader);
	            Sala s = deserializer_sala.next();
	            deserializer_sala.close(true);
	            System.out.println(s.getDescricao());
	            
	            this.setSalas(s);
	            
	            break;
	        case "DISCIPLINA":
	            Deserializer deserializer_disciplina = CsvIOFactory.createFactory(config,Disciplina.class).createDeserializer();
	            StringReader reader_disciplina = new StringReader(linha);
	            deserializer_disciplina.open(reader_disciplina);
	            Disciplina d = deserializer_disciplina.next();
	            deserializer_disciplina.close(true);
	            System.out.println(d.getDescricao());
	            
	            d.consertar_curso();
	            System.out.println(d.getCurso().getNome());
	            
	            this.setDisciplinas(d);
	            break;
	        case "ESTUDANTE":
	            Deserializer deserializer_estudante = CsvIOFactory.createFactory(config,Estudante.class).createDeserializer();
	            StringReader reader_estudante = new StringReader(linha);
	            deserializer_estudante.open(reader_estudante);
	            Estudante e = deserializer_estudante.next();
	            deserializer_estudante.close(true);
	            
	            e.consertar_disciplinas_cursar();
	            this.setEstudantes(e);
	            System.out.println(e.codigo);
	            
	            break;
	         case "PROFESSOR":
	        	System.out.println(linha);
	            Deserializer deserializer_professor = CsvIOFactory.createFactory(config,Professor.class).createDeserializer();
	            StringReader reader_professor = new StringReader(linha);
	            deserializer_professor.open(reader_professor);
	            Professor p = deserializer_professor.next();
	            deserializer_professor.close(true);
	            
	            p.consertar_disciplinas_ministrar();
	            this.setProfessor(p);
				System.out.println(Arrays.toString(p.getDisciplinas_a_ministrar().toArray()));		            
	            break;
	        default:
	        	System.err.println("Erro no parse de entidade");
		}
	}

	// lê o arquivo de informações, cria os objetos e os posiciona dentro do cromossomo
	public void ler_arquivo(){
		//Scanner reader = new Scanner(System.in); 
		//System.out.println("Informe a localização do arquivo: ");
		
	    //String  csvFile = reader.nextLine();
		//String  csvFile = "/home/eder/Documents/IC/timetabling/ag-informacoes.csv";
		String  csvFile = "/home/mrluizandre/Documents/IC/timetabling/ag-informacoes.csv";
	    
	    File arquivoCsv = new File(csvFile);
	    String entidade = null;
	    try {
	        String secaoArquivo = new String();
	        String linhasArquivo = new String();
	        
	        Scanner leitor = new Scanner(arquivoCsv);
	        
	        while(leitor.hasNext()){
	            linhasArquivo = leitor.nextLine();
	            
	            String [] arquivo = linhasArquivo.split("\n");
	            ler_proxima_linha:
	            for(String linha : arquivo  ){
	            	
	            	if(linha.startsWith("//")){
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("TIMESLOT")){
	            		entidade = "TIMESLOT";
	            		break ler_proxima_linha;
	            	}
	            	else if(linha.startsWith("CURSO")){
	            		entidade = "CURSO";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("TIPO DE SALA")){
	            		entidade = "TIPO DE SALA";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("SALA")){
	            		entidade = "SALA";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("DISCIPLINA")){
	            		entidade = "DISCIPLINA";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("ESTUDANTE")){
	            		entidade = "ESTUDANTE";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else if(linha.startsWith("PROFESSOR")){
	            		entidade = "PROFESSOR";
	            		break ler_proxima_linha;	            	   
	            	}
	            	else{
	            		criar_objetos(entidade, linha);
	            		//System.out.println(entidade);
	            	}
	            }
	        }	
	    }
	    catch (FileNotFoundException e) {
	    	System.err.println("Arquivo não encontrado!");
	    }
	}
	
	// calcula o fitness do cromossomo
	public int fitness(){
		int score = 10000;
		
		//tira 10 pontos por cada disciplina não ministrada
		score -= (this.disciplinas_ec.size() + this.disciplinas_ee.size() + this.disciplinas_em.size())*10 - this.disciplicas_alocadas*10;
		
		//tira 2 pontos para cada disciplina que um estudante precisa e não cursa
		for(Estudante e: estudantes){
			score -= (e.getDisciplinas_a_cursar().size() - e.getHorarios_ocupados().size())*2;
		}
 
		System.out.println("Fitness " + score);
		return score;
	}
	
	// verifica se lista contém item
	private boolean isInList(ArrayList<Integer> array, int n){
		for ( int i : array ) {
			if (i == n) {
				return true;
			}
		}
		return false;		
	}
	
	
}