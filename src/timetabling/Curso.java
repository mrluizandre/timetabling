package timetabling;

public class Curso {
	private int codigo;
	private String nome;
	private int[] turno;
	private int[] timeslots;
	private int numeroAlunos = 0;
	
	public Curso(int codigo, String nome, int[] turno, int[] timeslots) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.turno = turno;
		this.timeslots = timeslots;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int[] getTurno() {
		return turno;
	}
	public void setTurno(int[] turno) {
		this.turno = turno;
	}
	public int[] getTimeslots() {
		return timeslots;
	}
	public void setTimeslots(int[] timeslots) {
		this.timeslots = timeslots;
	}
	public int getNumeroAlunos() {
		return numeroAlunos;
	}
	public void setNumeroAlunos(int numeroAlunos) {
		this.numeroAlunos = numeroAlunos;
	}

}
