package timetabling;

import java.util.ArrayList;

import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Estudante {
	
	@CsvField(pos = 1, converterType = IntegerConverter.class)
	public int codigo;
	
	@CsvField(pos = 2)
	private String nome;
	
	@CsvField(pos = 3, converterType = IntegerConverter.class)
	private int d1;
	@CsvField(pos = 4, converterType = IntegerConverter.class)
	private int d2;
	@CsvField(pos = 5, converterType = IntegerConverter.class)
	private int d3;
	@CsvField(pos = 6, converterType = IntegerConverter.class)
	private int d4;
	@CsvField(pos = 7, converterType = IntegerConverter.class)
	private int d5;
	@CsvField(pos = 8, converterType = IntegerConverter.class)
	private int d6;
	@CsvField(pos = 9, converterType = IntegerConverter.class)
	private int d7;
	@CsvField(pos = 10, converterType = IntegerConverter.class)
	private int d8;
	@CsvField(pos = 11, converterType = IntegerConverter.class)
	private int d9;
	@CsvField(pos = 12, converterType = IntegerConverter.class)
	private int d10;	
	
	private ArrayList<Integer> disciplinas_a_cursar = new ArrayList<Integer>();

	private ArrayList<Integer> horarios_ocupados =  new ArrayList<Integer>();
	
//	public Estudante(int codigo, String nome, Disciplina[] disciplinas_a_cursar) {
//		super();
//		this.codigo = codigo;
//		this.nome = nome;
//		this.disciplinas_a_cursar = disciplinas_a_cursar;
//	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getD1() {
		return d1;
	}
	public int getD2() {
		return d2;
	}
	public int getD3() {
		return d3;
	}
	public int getD4() {
		return d4;
	}
	public int getD5() {
		return d5;
	}
	public int getD6() {
		return d6;
	}
	public int getD7() {
		return d7;
	}
	public int getD8() {
		return d8;
	}
	public int getD9() {
		return d9;
	}
	public int getD10() {
		return d10;
	}
	public ArrayList<Integer> getDisciplinas_a_cursar() {
		return disciplinas_a_cursar;
	}
	public void setDisciplinas_a_cursar(ArrayList<Integer> disciplinas_a_cursar) {
		this.disciplinas_a_cursar = disciplinas_a_cursar;
	}
	public ArrayList<Integer> getHorarios_ocupados() {
		return horarios_ocupados;
	}
	public void setHorario_ocupados(int horarios_ocupados) {
		this.horarios_ocupados.add(horarios_ocupados);
	}
	
	//adicionar todas as disciplinas válidas num único horário
	public void consertar_disciplinas_cursar(){
		if(this.d1 != 0){
			this.disciplinas_a_cursar.add(this.d1);
		}
		if(this.d2 != 0){
			this.disciplinas_a_cursar.add(this.d2);
		}
		if(this.d3 != 0){
			this.disciplinas_a_cursar.add(this.d3);
		}
		if(this.d4 != 0){
			this.disciplinas_a_cursar.add(this.d4);
		}
		if(this.d5 != 0){
			this.disciplinas_a_cursar.add(this.d5);
		}
		if(this.d6 != 0){
			this.disciplinas_a_cursar.add(this.d6);
		}
		if(this.d7 != 0){
			this.disciplinas_a_cursar.add(this.d7);
		}
		if(this.d8 != 0){
			this.disciplinas_a_cursar.add(this.d8);
		}
		if(this.d9 != 0){
			this.disciplinas_a_cursar.add(this.d9);
		}
		if(this.d10 != 0){
			this.disciplinas_a_cursar.add(this.d10);
		}
	}
	
}
