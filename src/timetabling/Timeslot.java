package timetabling;

import java.util.ArrayList;

import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

//@CsvDataType()
public class Timeslot {
//    @CsvField(pos = 1, converterType = IntegerConverter.class)
//	private int codigo;
//    
//    @CsvField(pos = 3)
//	private String horario;
	
	private Sala sala;
	private Disciplina disciplina;
	private Professor professor;
	private ArrayList<Estudante> estudantes;
	

//	public Timeslot(Sala sala, Disciplina disciplina, Professor professor, ArrayList<Estudante> estudantes) {
//		super();
//		this.sala = sala;
//		this.disciplina = disciplina;
//		this.professor = professor;
//		this.estudantes = estudantes;
//	}
//	public int getCodigo() {
//		return codigo;
//	}
//	public void setCodigo(int codigo) {
//		this.codigo = codigo;
//	}
//	public String getHorario() {
//		return horario;
//	}
//	public void setHorario(String horario) {
//		this.horario = horario;
//	}
	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public ArrayList<Estudante> getEstudantes() {
		return estudantes;
	}
	public void setEstudantes(ArrayList<Estudante> estudantes) {
		this.estudantes = estudantes;
	}
}
