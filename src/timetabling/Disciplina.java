package timetabling;

import org.jsefa.common.converter.IntegerConverter;
import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Disciplina {
	
	@CsvField(pos = 1, converterType = IntegerConverter.class)
	private int codigo;
	
	@CsvField(pos = 2, converterType = IntegerConverter.class)
	private int codigo_curso;
	
	@CsvField(pos = 3)
	private int periodo;
	
	@CsvField(pos = 4)
	private String descricao;
	
	@CsvField(pos = 5)
	private int carga_horaria_teoria;
	
	@CsvField(pos = 6)
	private int tipo_sala_teorica;
	
	@CsvField(pos = 7)
	private int carga_horaria_pratica;
	
	@CsvField(pos = 8)
	private int tipo_sala_pratica;
	
	private Curso curso;
	
	private boolean restricao_horario;
	private int[] horario_pode;
	private int numeroPedidos = 0;
	
	int[] horarios_ec = {32,33,34,35,36,37,43,45,45,46,56,57,58,59,60,61,67,68,69,70,80,81,82,83,84,85,91,92,93,94,104,105,106,107,108,109,115,116,117,118,128,129,130,131,132,133,139,140,141,142,152,153,154,155,156,157};
	int[] horarios_ee = {32,33,34,35,36,37,38,39,40,41,42,43,45,45,46,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,152,153,154,155,156,157};
	int[] horarios_em = {43,44,45,46,67,68,69,70,91,92,93,94,115,116,117,118,139,140,141,142,152,153,154,155,156,157};
	
	int[] turnos_ec = {1,3};
	int[] turnos_ee = {1,2};
	int[] turnos_em = {3};
	
	Curso ec = new Curso(1,"Engenhaira de Computação",turnos_ec,horarios_ec);
	Curso ee = new Curso(2,"Engenharia Elétrica",turnos_ee, horarios_ee);
	Curso em = new Curso(3,"Engenharia Mecânica",turnos_em,horarios_em);

	
	public void consertar_curso(){
		if(this.codigo_curso == 1)
			this.curso = ec;
		else if(this.codigo_curso == 2)
			this.curso = ee;
		else if(this.codigo_curso == 3)
			this.curso = em;
	}
	
//	public Disciplina(int codigo, Curso curso, String descricao, int carga_horaria_teoria, int carga_horaria_pratica,
//			int tipo_sala_pratica, int periodo, boolean restricao_horario) {
//		super();
//		this.codigo = codigo;
//		this.curso = curso;
//		this.descricao = descricao;
//		this.carga_horaria_teoria = carga_horaria_teoria;
//		this.carga_horaria_pratica = carga_horaria_pratica;
//		this.tipo_sala_pratica = tipo_sala_pratica;
//		this.periodo = periodo;
//		this.restricao_horario = restricao_horario;
//	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getCarga_horaria_teoria() {
		return carga_horaria_teoria;
	}
	public void setCarga_horaria_teoria(int carga_horaria_teoria) {
		this.carga_horaria_teoria = carga_horaria_teoria;
	}
	public int getCarga_horaria_pratica() {
		return carga_horaria_pratica;
	}
	public void setCarga_horaria_pratica(int carga_horaria_pratica) {
		this.carga_horaria_pratica = carga_horaria_pratica;
	}
	public int getTipo_sala_pratica() {
		return tipo_sala_pratica;
	}
	public void setTipo_sala_pratica(int tipo_sala_pratica) {
		this.tipo_sala_pratica = tipo_sala_pratica;
	}
	public int getPeriodo() {
		return periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public boolean isRestricao_horario() {
		return restricao_horario;
	}
	public void setRestricao_horario(boolean restricao_horario) {
		this.restricao_horario = restricao_horario;
	}
	public int[] getHorario_pode() {
		return horario_pode;
	}
	public void setHorario_pode(int[] horario_pode) {
		this.horario_pode = horario_pode;
	}
	public int getNumeroPedidos() {
		return numeroPedidos;
	}
	public void setNumeroPedidos(int numeroPedidos) {
		this.numeroPedidos = numeroPedidos;
	}
}
